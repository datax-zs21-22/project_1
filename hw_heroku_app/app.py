import pandas as pd
import streamlit as st





st.title("Gitlab - Streamlit ML Application")
st.write("Deployed using Gitlab CI/CD  and Heroku")

words = ['university', 'college', 'school', 'army', 'tank', 'aircraft',
         'cat', 'dog', 'lion', 'puppy', 'student', 'teacher',
         'lesson', 'mobile', 'kitten']

corpus = ["prince", "princess", "nurse", "doctor", "banker", "man", "woman",
         "cousin", "neice", "king", "queen", "dude", "guy", "gal", "fire",
         "dog", "cat", "mouse", "red", "blue", "green", "yellow", "water",
         "person", "family", "brother", "sister"]

df = pd.DataFrame(corpus, columns=['words'])
st.dataframe(df)
